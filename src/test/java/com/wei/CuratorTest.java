package com.wei;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class CuratorTest {

    private CuratorFramework client;

    /**
     * @author Wei
     * @date 2021/7/23 10:34
     * @description 建立连接
     **/
    @Test
    @Before
    public void testConnect() {
        // 重试次数
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(3000, 10);
        // 第一种方式
        // CuratorFramework client = CuratorFrameworkFactory.newClient(
        //         "localhost:2181",
        //         60 * 1000,
        //         15 * 1000,
        //         retryPolicy
        // );
        // 第二种方式
        this.client = CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(60 * 1000)
                .connectionTimeoutMs(15 * 1000)
                .retryPolicy(retryPolicy)
                .namespace("wei")
                .build();

        // 开启连接
        this.client.start();
    }

    /**
     * @author Wei
     * @date 2021/7/23 10:45
     * @description 创建节点
     **/
    @Test
    public void testCreate() throws Exception {
        // 基本创建，默认值为当前客户端ip地址
        String path = client.create()
                .forPath("/app2");
        System.out.println(path);
    }

    @Test
    public void testCreate1() throws Exception {
        // 创建节点，带数据
        String path = client.create()
                .forPath("/app2", "haha".getBytes(StandardCharsets.UTF_8));
        System.out.println(path);
    }

    @Test
    public void testCreate2() throws Exception {
        // 设置节点类型 （默认：持久化）
        String path = client.create()
                .withMode(CreateMode.EPHEMERAL) // 设置临时模式
                .forPath("/app3");
        System.out.println(path);
    }

    @Test
    public void testCreate3() throws Exception {
        // 创建多节点
        String path = client.create()
                .creatingParentContainersIfNeeded() // 父节点不存在，则创建父节点
                .forPath("/app4/p2");
        System.out.println(path);
    }

    /**
     * @author Wei
     * @date 2021/7/23 11:03
     * @description 查询数据
     **/
    @Test
    public void testGet() throws Exception {
        byte[] data = client.getData().forPath("/app1");
        System.out.println(new String(data));
    }

    @Test
    public void testGet1() throws Exception {
        // 查询子节点
        List<String> path = client.getChildren().forPath("/app4");
        System.out.println(path);
    }

    @Test
    public void testGet2() throws Exception {
        // 查询节点状态 ls -s
        Stat stat = new Stat();
        client.getData().storingStatIn(stat).forPath("/app1");
        System.out.println(stat);
    }

    /**
     * @author Wei
     * @date 2021/7/23 11:15
     * @description 修改节点数据
     **/
    @Test
    public void testSet() throws Exception {
        client.setData().forPath("/app2", "哈哈".getBytes(StandardCharsets.UTF_8));
    }

    /**
     * @author Wei
     * @date 2021/7/23 11:18
     * @description 根据版本修改数据
     **/
    @Test
    public void testSetForVersion() throws Exception {
        Stat stat = new Stat();
        client.getData().storingStatIn(stat).forPath("/app2");

        client.setData()
                .withVersion(stat.getVersion()) // 版本号
                .forPath("/app2", "哈哈123".getBytes(StandardCharsets.UTF_8));
    }

    /**
     * @author Wei
     * @date 2021/7/23 11:23
     * @description 删除节点
     **/
    @Test
    public void testDelete() throws Exception {
        // 删除单个节点
        client.delete().forPath("/app1");
    }

    @Test
    public void testDelete1() throws Exception {
        // 删除带子节点的节点
        client.delete().deletingChildrenIfNeeded().forPath("/app4");
    }

    @Test
    public void testDelete3() throws Exception {
        // 必须删除成功
        client.delete().guaranteed().forPath("/app2");
    }

    @Test
    public void testDelete4() throws Exception {
        // 回调
        client.delete()
                .guaranteed()
                .inBackground((curatorFramework, curatorEvent) -> {
                    System.out.println("删除成功...");
                    System.out.println(curatorEvent);
                })
                .forPath("/app2");
    }
    
    @After
    public void close() {
        if (client != null) {
            client.close();
        }
    }
}
